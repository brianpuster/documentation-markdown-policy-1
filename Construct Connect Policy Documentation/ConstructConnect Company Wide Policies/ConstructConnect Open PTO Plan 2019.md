title: OPEN PTO PLAN 2019
published: 2019-07-11
author: Julie Storm - Chief People Officer

## Our Policy

- This policy offers unlimited PTO days - take the time you need to operate at peak performance. 
- You do not "accrue" PTO days as in traditional plans, and will not be compensated for "unused" PTO time upon termination.
- In the event you must be off work for an extended period of time due to illness or injury (Leave of Absence), Open PTO will apply for the first 2 weeks. Short or Long-Term Disability must be utilized for the remainder of the time off.  This time off will run in conjunction with FMLA, beginning on your first day off.
- If the time off is for the birth of a child, time off will be taken as follows:
    - For maternity leave it will be paid at 100% as follows: 
        - 6 weeks of paid Maternity Leave followed by 2 weeks of Paternal Leave and then 2 weeks of Open PTO for a total of 10 weeks of maternity leave benefits
    - For Paternal Leave it will be paid at 100% as follows: 
        - 2 weeks of paid Paternal Leave followed by 2 weeks of Open PTO for a total of 4 weeks of paternal leave benefits 
- All Maternity and Paternal Leave will run in conjunction with FMLA from the first day off.

## Your Responsibilities

- The time you take off should be reasonable and rational.
- Let your manager know at least 2 weeks ahead of schedule (when at all possible) that you'll be on PTO, and report in on days you need to be out unexpectedly. 
- Be available when you're needed. Customer calls, staff meetings and other time sensitive responsibilities must be covered regardless of your personal PTO schedule.
- Be productive. You are employed by ConstructConnect as a full-time employee, and are expected to contribute as such.
- If job performance standards aren't being met, set schedules are not being worked, or if time off is excessive in the view of your manager, the privilege may be revoked.  If this occurs you will be reverted to the traditional PTO plan and given the hours you have accrued and not taken at that point in the calendar year.


☐  I elect to participate in the Open PTO program for 2019 and agree to the terms above  

☐  I elect to participate in the standard PTO program for 2019.
