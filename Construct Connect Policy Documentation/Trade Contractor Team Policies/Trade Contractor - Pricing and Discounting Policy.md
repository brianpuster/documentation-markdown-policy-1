title: Trade Contractor - Pricing and Discounting Policy
author: Steve Testa - VP Strategic Initiatives and Continuous Improvement
published: 2019-11-20
effective date: 2019-12-01

---

#### Policy Level
- Very Important

#### Approver(s)
- Jim Hill – EVP & GM Trade Contractor 
- Buck Brody - EVP & GM Finance

#### Location or Team Applicability
- United States based core Trade Contractor new sales, all locations 

---

#### Policy Text:
**Quotes must be physically approved in the system by a person who has the authority level to approve it. Comments stating “approval was received” is not sufficient. Closed Opportunities will be audited for compliance and exceptions will be reported.**
 
Pricing and Discounting approval authority levels:
 
- Sales Rep - </= 10% of pricing calculator Final Annualized Grand Total
- Sales Manager/Director - </= 20% of pricing calculator Final Annualized Grand Total
- VP of Sales - >20% of pricing calculator Final Annualized Grand Total
 
Violations to the pricing and discounting policy will be handled as follows:
 
- 1st offense – No commission on the deal and a verbal warning
- 2nd offense – No commission on the deal and a written warning
- 3rd offense – No commission on the deal and a final written warning
- 4th offense – No commission on the deal and termination

---