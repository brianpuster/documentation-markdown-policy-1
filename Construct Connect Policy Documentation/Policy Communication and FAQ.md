title: Policy Communication and FAQ
author: Steve Testa - VP Strategic Initiatives and Continuous Improvement
published: 2020-08-14


As we continue to align as one company and one brand, it is important that we build structure and consistency across the entire organization. In October 2019, we established a process and standard for policy documentation. This central [Policy Documentation Repository](https://policydocs.buildone.co/pages/policy) serves as a single location to access and/or link to company policies that impact all of ConstructConnect, including team specific policies.

Though this was previously communicated at the ELT/SLT level, we are now rolling this out company-wide. This repository will serve as a single source of truth for all things regarding policy, and team members will be able to reference this repository for answers to any policy questions they may have. Given our remote working environment, the policy repository makes quick reference easy and feasible for everyone to go to first.

A few advantages of having all of our policies documented in one place are:

- Scale: Rather than having to explain policies to all team members, it is more effective/efficient to have a document(s) that an employee can read.
- Clarity: A document helps to ensure that everyone gets the same message & expectation.
- Asynchronous: A document can be consumed at the time and pace that work for the reader.

Please note: It is each team member's responsibility to familiarize with and adhere to all applicable policies. To access the [Policy Documentation Repository](https://policydocs.buildone.co/pages/policy), you must be connected to our network through VPN or at an office location. Additionally, existing policies on The Loop are being left in place, but the repository will include links to them.  

For questions, please reference the FAQ below. Questions not covered can be directed to Steve Testa or Mike DiChiara.
 

Thank you -


# FAQ:

## Who does this apply to?

This applies to all team members within the company. Should you have questions about which policies are applicable to your role, please contact your manager, Steve Testa or Mike DiChiara.

## Who is allowed to view this content?

Content created in the [Policy Documentation Repository](https://policydocs.buildone.co/pages/policy) is accessible to anyone in the company. Sensitive policy documentation for specific audiences will be stored in a secure location and linked to from the Policy Documentation Repository.

## What software do I need to view the content?

No special software is needed, but you do need to be on VPN to access the [Policy Documentation Repository](https://policydocs.buildone.co/pages/policy).

## Does this replace The Loop or the company handbook?

No. This is more narrowly focused than The Loop.  However, we will migrate some items from The Loop and/or link to The Loop from the [Policy Documentation Repository](https://policydocs.buildone.co/pages/policy). We will be migrating the handbook to the [Policy Documentation Repository](https://policydocs.buildone.co/pages/policy) over the coming months.

## How do I create new policies?

We have a policy for creating new policies! You can find the document here: [Creating Policy](https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/ConstructConnect%20Company%20Wide%20Policies/ConstructConnect%20Policy%20for%20Creating%20Policy)

## Does my team need to move all existing policies to this location?

You are encouraged to, but it is not required at this time. For questions or assistance with adding policies to the repository, please contact Steve Testa.