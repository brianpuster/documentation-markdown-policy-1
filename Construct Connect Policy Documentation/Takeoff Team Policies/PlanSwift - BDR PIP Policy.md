title: PlanSwift - BDR PIP Policy
published: 2019-09-11
author: Jim Hill - EVP and GM Trade Contractor

**PlanSwift Business Development Representative**

**Performance Improvement Plan Process**

Business Development Representatives' performance will be reviewed by
Sales Management on a monthly basis at the beginning of each month for
the previous month's performance against the individual's established
minimum monthly threshold. If a business development representative does
not meet their full threshold, the performance improvement plan process
will begin as outlined below. Business Development Representatives who
have not yet completed the 90-day orientation period will not be subject
to this process, but will be held responsible for a successful
orientation period, which consists of:

**Orientation Period Performance Guidelines**

-   Satisfactory attendance -- outlined in the Employment Handbook

-   Attitude that is open coaching and feedback

-   Achieve a minimum of 60% of the attended demo ramp metric goal each
    month

-   Completion of all required Certifications

*If team members are unable to demonstrate their ability to achieve a
satisfactory level of performance during the orientation period, their
employment could be terminated.*

*Upon successful completion of the orientation period team members will
enter the ongoing employment classification below*

**Performance Improvement Process Post Orientation Period**

-   Performance below 60% of average quota for two consecutive months:
    1st Stage Improvement Plan

-   Performance below 60% for a rolling three-month period: 2nd Stage
    Improvement Plan. Missed goal below 50%: up to and including
    termination.

-   While on 2nd Stage Improvement Plan you MUST hit minimum 65% for
    rolling three-month period. Failure to do so will result in
    termination.

-   Stages run consecutively.

-   In order to be removed from performance improvement plan you must
    maintain 75% of goal for rolling three-month period for two
    consecutive months.

Management reserves the right to by-pass stages of the improvement
process depending on the nature of the issue.

Minimum monthly threshold as it pertains to the performance plan will be
pro-rated for approved FMLA or bereavement absences.

Business Development Representatives with planned paid time off (PTO)
scheduled, and approved, at least one month in advance for a minimum of
4 consecutive days will only be responsible for the pro-rated portion of
the goal associated with the number of business days worked. 

This is a guideline for a performance improvement plan and is subject to
change at the discretion of management.

I have read, and acknowledge, the PlanSwift Business Development
Representative Performance Improvement Plan Process to be the guideline
to which I will be held accountable regarding my performance.
